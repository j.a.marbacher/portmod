from tempfile import mkdtemp
from test.env import select_profile, setup_env
from test.merge import merge

from pytest import fail

from portmod._gui.config import Config
from portmod._gui.Manage.InstalledPackagesModel import InstalledPackagesProxyModel
from portmod._gui.packages import get_installed_packages, get_local_flags
from portmod.globals import env
from portmod.loader import load_installed_pkg
from portmod.prefix import add_prefix
from portmodlib.atom import Atom


# NOTE: I don't like how this has to be called for every test,
# but it breaks if it isn't.
def get_model() -> InstalledPackagesProxyModel:
    setup_env("test")
    merge(["test/test4"], nodeps=True)
    merge(["test/test"], nodeps=True)

    return InstalledPackagesProxyModel(get_installed_packages())


def test_init():
    model = get_model()
    assert model.realModel._data == get_installed_packages()
    assert model.realModel._data == [
        load_installed_pkg(Atom("test/test")),
        load_installed_pkg(Atom("test/test4")),
    ]


def test_get_atom():
    model = get_model()
    assert model.getAtom(0) == "test/test"
    assert model.getAtom(1) == "test/test4"


def test_get_name():
    model = get_model()
    assert model.get_name(0) == "Test"


def test_get_author():
    model = get_model()
    assert (
        model.get_author(0)
        == "someone <someone@example.org>, someone else, justamail@example.org, just a string"
    )


def test_get_version():
    model = get_model()
    assert model.get_version(0) == "2.0"


def test_get_size():
    model = get_model()
    assert model.get_size(0) == "0.0 B"


def test_get_license():
    model = get_model()
    assert model.get_license(0) == "test"


def test_get_description():
    model = get_model()
    assert model.get_description(0) == "Test Desc: foo-bar"


def test_get_homepage():
    model = get_model()
    assert model.get_homepage(0) == "https://example.org"


def test_get_local_flags_model():
    model = get_model()
    installed_pybuild = load_installed_pkg(Atom("test/test4"))
    if installed_pybuild:
        flags = get_local_flags(installed_pybuild)
        flags_model = model.get_local_flags_model(1)
        assert flags_model._data == flags
    else:
        fail()


def test_change_to_current_prefix():
    model = get_model()

    temp2 = mkdtemp("portmod.test2")
    add_prefix("test2", "test", temp2, "test", ["test"])
    select_profile("test")

    merge(["test/test7"], nodeps=True)

    # Adding a new prefix sets the current prefix to that one, so set it back.
    env.set_prefix("test")

    assert model.realModel._data == [
        load_installed_pkg(Atom("test/test")),
        load_installed_pkg(Atom("test/test4")),
    ]

    gui_config = Config()
    gui_config.set_prefix("test2")

    model.changeToCurrentPrefix()

    assert model.realModel._data == [
        load_installed_pkg(Atom("test/test7")),
    ]
