Concepts
========

These are various concepts relevant to understanding how portmod works. It is recommended that you familiarise yourself with them before using Portmod.

.. toctree::
   :hidden:

   keywords
   sets
   use-flags
   sandbox
   cfg-protect
   modules

..
   The below table is designed to include the first sentence of the file following the header
   Note that it is not particularly stable, but it at least break noisily if the descriptions
   are changed such that it no longer works. Using comments may be necessary to find
   the end of the text to include

.. list-table::
   :widths: 25 75
   :header-rows: 0

   * - :ref:`keywords`
     - .. include:: keywords.rst
          :start-after: ========
          :end-before: .
   * - :ref:`sets`
     - .. include:: sets.rst
          :start-after: ====
          :end-before: .
   * - :ref:`use-flags`
     - .. include:: use-flags.rst
          :start-after: =========
          :end-before: .
   * - :ref:`sandbox`
     - .. include:: sandbox.rst
          :start-after: =======
          :end-before: #comment
   * - :ref:`cfg-protect`
     - .. include:: cfg-protect.rst
          :start-after: ========================
          :end-before: .
   * - :ref:`concepts-modules`
     - .. include:: modules.rst
          :start-after: =======
          :end-before: .
