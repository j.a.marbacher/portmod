from test.env import setup_env
from test.merge import merge

from pytest import fail

from portmod._gui.packages import get_installed_packages, get_local_flags
from portmod.loader import load_installed_pkg
from portmodlib.atom import Atom


def test_get_installed_packages():
    setup_env("test")
    merge(["test/test0"], nodeps=True)
    merge(["test/test2"], nodeps=True)
    merge(["test/test3"], nodeps=True)

    packages = [
        load_installed_pkg(Atom("test/test3")),
        load_installed_pkg(Atom("test/test2")),
        load_installed_pkg(Atom("test/test0")),
    ]

    assert get_installed_packages() == packages


def test_get_local_flags():
    setup_env("test")
    merge(["test/test4"], nodeps=True)

    installed_pybuild = load_installed_pkg(Atom("test/test4"))
    if installed_pybuild:
        assert get_local_flags(installed_pybuild) == {"baf": (False, "test flag")}
    else:
        fail()
